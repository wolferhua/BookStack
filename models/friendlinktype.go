package models

import (
	"fmt"

	"github.com/astaxie/beego/orm"
)

//友链数据表
type FriendLinkType struct {
	Id   int    //自增主键
	Sort int    //排序
	Name string `orm:"unique;size(128)"` //链接地址
}

//添加友情链接
func (this *FriendLinkType) Add(Name string) (err error) {
	var flt = FriendLinkType{
		Name: Name,
		Sort: 0,
	}
	_, err = orm.NewOrm().Insert(&flt)
	return
}

//根据字段更新友链
func (this *FriendLinkType) Update(id int, field string, value interface{}) (err error) {
	sql := fmt.Sprintf("update md_friend_link_type set %v=? where id=?", field)
	_, err = orm.NewOrm().Raw(sql, value, id).Exec()
	return
}

//删除友情链接
func (this *FriendLinkType) Del(id int) (err error) {
	var link = FriendLinkType{Id: id}
	_, err = orm.NewOrm().Delete(&link)
	return
}

//查询友链列表
//all表示是否查询全部，当为false时，只查询启用状态的友链，否则查询全部
func (this *FriendLinkType) GetList(all bool) (links []FriendLinkType) {
	qs := orm.NewOrm().QueryTable("md_friend_link_type")

	qs.OrderBy("sort").All(&links)
	return
}
