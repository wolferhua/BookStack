package models

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"time"
)

// Document struct.
type DocumentView struct {
	DocumentId        int           `orm:"pk;auto;column(document_id)" json:"doc_id"`
	DocumentName      string        `orm:"column(document_name);size(500)" json:"doc_name"`
	Identify          string        `orm:"column(identify);size(100);index;null;default(null)" json:"identify"` // Identify 文档唯一标识
	BookId            int           `orm:"column(book_id);type(int);index" json:"book_id"`
	ParentId          int           `orm:"column(parent_id);type(int);index;default(0)" json:"parent_id"`
	OrderSort         int           `orm:"column(order_sort);default(0);type(int);index" json:"order_sort"`
	Release           string        `orm:"column(release);type(text);null" json:"release"` // Release 发布后的Html格式内容.
	CreateTime        time.Time     `orm:"column(create_time);type(datetime);auto_now_add" json:"create_time"`
	MemberId          int           `orm:"column(member_id);type(int)" json:"member_id"`
	ModifyTime        time.Time     `orm:"column(modify_time);type(datetime);default(null);auto_now" json:"modify_time"`
	ModifyAt          int           `orm:"column(modify_at);type(int)" json:"-"`
	Version           int64         `orm:"type(bigint);column(version)" json:"version"`
	AttachList        []*Attachment `orm:"-" json:"attach"`
	Vcnt              int           `orm:"column(vcnt);default(0)" json:"vcnt"` //文档项目被浏览次数
	Markdown          string        `orm:"-" json:"markdown"`
	BookName          string        `orm:"column(book_name);size(500)" json:"book_name"`                // BookName 项目名称.
	BookIdentify      string        `orm:"column(book_identify);size(100);unique" json:"book_identify"` // Identify 项目唯一标识.
	OrderIndex        int           `orm:"column(order_index);type(int);default(0)" json:"order_index"`
	Description       string        `orm:"column(description);size(2000)" json:"description"` // Description 项目描述.
	Label             string        `orm:"column(label);size(500)" json:"label"`
	PrivatelyOwned    int           `orm:"column(privately_owned);type(int);default(0)" json:"privately_owned"` // PrivatelyOwned 项目私有： 0 公开/ 1 私有
	PrivateToken      string        `orm:"column(private_token);size(500);null" json:"private_token"`           // 当项目是私有时的访问Token.
	Status            int           `orm:"column(status);type(int);default(0)" json:"status"`                   //状态：0 正常/1 已删除
	Editor            string        `orm:"column(editor);size(50)" json:"editor"`                               //默认的编辑器.
	DocCount          int           `orm:"column(doc_count);type(int)" json:"doc_count"`                        // DocCount 包含文档数量.
	CommentStatus     string        `orm:"column(comment_status);size(20);default(open)" json:"comment_status"` // CommentStatus 评论设置的状态:open 为允许所有人评论，closed 为不允许评论, group_only 仅允许参与者评论 ,registered_only 仅允许注册者评论.
	CommentCount      int           `orm:"column(comment_count);type(int)" json:"comment_count"`
	Cover             string        `orm:"column(cover);size(1000)" json:"cover"`                                        //封面地址
	Theme             string        `orm:"column(theme);size(255);default(default)" json:"theme"`                        //主题风格
	BookCreateTime    time.Time     `orm:"type(book_datetime);column(create_time);auto_now_add" json:"book_create_time"` // CreateTime 创建时间 .
	BookMemberId      int           `orm:"column(book_member_id);size(100)" json:"book_member_id"`
	BookModifyTime    time.Time     `orm:"type(datetime);column(book_modify_time);auto_now_add" json:"book_modify_time"`
	ReleaseTime       time.Time     `orm:"type(datetime);column(release_time);" json:"release_time"`   //项目发布时间，每次发布都更新一次，如果文档更新时间小于发布时间，则文档不再执行发布
	GenerateTime      time.Time     `orm:"type(datetime);column(generate_time);" json:"generate_time"` //下载文档生成时间
	LastClickGenerate time.Time     `orm:"type(datetime);column(last_click_generate)" json:"-"`        //上次点击上传文档的时间，用于显示频繁点击浪费服务器硬件资源的情况
	BookVersion       int64         `orm:"type(bigint);column(book_version);default(0)" json:"book_version"`
	BookVcnt          int           `orm:"column(vcnt);default(0)" json:"vcnt"`    //文档项目被阅读次数
	Star              int           `orm:"column(star);default(0)" json:"star"`    //文档项目被收藏次数
	Score             int           `orm:"column(score);default(40)" json:"score"` //文档项目评分，默认40，即4.0星
	CntScore          int           //评分人数
	CntComment        int           //评论人数
}

func NewDocumentView() *DocumentView {
	return new(DocumentView)
}

func (this *DocumentView) FindForHomeToPager(pageIndex, pageSize, member_id int, orderType string) (books []*DocumentView, totalCount int, err error) {
	qb, err := orm.NewQueryBuilder(beego.AppConfig.String("db_adapter"))
	if err != nil {
		return
	}
	dtname := NewDocument().TableNameWithPrefix()
	btname := NewBook().TableNameWithPrefix()

	fields := `book_id,
book_name,
identify,
order_index,
description,
label,
privately_owned,
private_token,
status,
editor,
doc_count,
comment_status,
comment_count,
cover,
theme,
create_time,
member_id,
modify_time,
release_time,
generate_time,
last_click_generate ,
version,
vcnt,
star,
score,`
	beego.Error("tags \n" + fields)
	qb.Select(fmt.Sprintf("%s.*", dtname))
	qb.From(dtname)
	qb.InnerJoin(btname).On(fmt.Sprintf("%s.book_id = %s.book_id", btname, dtname))
	//sql := qb.String()

	return
}
