package commands

import (
	"fmt"
	"os"
	"time"

	"strings"

	"github.com/TruthHun/BookStack/conf"
	"github.com/TruthHun/BookStack/models"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

//系统安装.
func Install() {

	fmt.Println("Initializing...")

	err := orm.RunSyncdb("default", false, true)
	fmt.Println("sync end...")
	if err == nil {
		initialization()
	} else {
		panic(err.Error())
		os.Exit(1)
	}
	initSeo()
	initLinks()
	fmt.Println("Install Successfully!")
	os.Exit(0)

}

func Version() {
	if len(os.Args) >= 2 && os.Args[1] == "version" {
		fmt.Println(conf.VERSION)
		os.Exit(0)
	}
}

//初始化数据
func initialization() {

	err := models.NewOption().Init()

	if err != nil {
		panic(err.Error())
		os.Exit(1)
	}

	member, err := models.NewMember().FindByFieldFirst("account", "admin")
	if err == orm.ErrNoRows {

		member.Account = "admin"
		member.Avatar = beego.AppConfig.String("avatar")
		member.Password = "admin"
		member.AuthMethod = "local"
		member.Nickname = "管理员"
		member.Role = 0
		member.Email = "bookstack@qq.cn"

		if err := member.Add(); err != nil {
			panic("Member.Add => " + err.Error())
			os.Exit(0)
		}

		book := models.NewBook()
		book.MemberId = member.MemberId
		book.BookName = "BookStack"
		book.Status = 0
		book.Description = "这是一个BookStack演示项目，该项目是由系统初始化时自动创建。"
		book.CommentCount = 0
		book.PrivatelyOwned = 0
		book.CommentStatus = "closed"
		book.Identify = "bookstack"
		book.DocCount = 0
		book.CommentCount = 0
		book.Version = time.Now().Unix()
		book.Cover = conf.GetDefaultCover()
		book.Editor = "markdown"
		book.Theme = "default"
		//设置默认时间，因为beego的orm好像无法设置datetime的默认值
		defaultTime, _ := time.Parse("2006-01-02 15:04:05", "2006-01-02 15:04:05")
		book.LastClickGenerate = defaultTime
		book.GenerateTime = defaultTime
		//book.ReleaseTime = defaultTime
		book.ReleaseTime, _ = time.Parse("2006-01-02 15:04:05", "2000-01-02 15:04:05")
		book.Score = 40

		if err := book.Insert(); err != nil {
			panic("Book.Insert => " + err.Error())
			os.Exit(0)
		}

	}
}

//初始化SEO
func initSeo() {
	sqlslice := []string{"insert ignore into `" + conf.GetDatabasePrefix() + "seo`(`id`,`page`,`statement`,`title`,`keywords`,`description`) values ('1','index','首页','{sitename}_分享，让知识传承更久远','{keywords}','{description}'),",
		"('2','label_list','标签列表页','{title} - {sitename}','{keywords}','{description}'),",
		"('3','label_content','标签内容页','{title} - {sitename}','{keywords}','{description}'),",
		"('4','book_info','文档信息页','{title} - {sitename}','{keywords}','{description}'),",
		"('5','book_read','文档阅读页','{title} - {sitename}','{keywords}','{description}'),",
		"('6','search_result','搜索结果页','{title} - {sitename}','{keywords}','{description}'),",
		"('7','user_basic','用户基本信息设置页','{title}  - {sitename}','{keywords}','{description}'),",
		"('8','user_pwd','用户修改密码页','{title}  - {sitename}','{keywords}','{description}'),",
		"('9','project_list','项目列表页','{title}  - {sitename}','{keywords}','{description}'),",
		"('11','login','登录页','{title} - {sitename}','{keywords}','{description}'),",
		"('12','reg','注册页','{title} - {sitename}','{keywords}','{description}'),",
		"('13','findpwd','找回密码','{title} - {sitename}','{keywords}','{description}'),",
		"('14','manage_dashboard','仪表盘','{title} - {sitename}','{keywords}','{description}'),",
		"('15','manage_users','用户管理','{title} - {sitename}','{keywords}','{description}'),",
		"('16','manage_users_edit','用户编辑','{title} - {sitename}','{keywords}','{description}'),",
		"('17','manage_project_list','项目列表','{title} - {sitename}','{keywords}','{description}'),",
		"('18','manage_project_edit','项目编辑','{title} - {sitename}','{keywords}','{description}'),",
		"('19','cate','书籍分类','{title} - {sitename}','{keywords}','{description}'),",
		"('20','ucenter-share','用户主页','{title} - {sitename}','{keywords}','{description}'),",
		"('21','ucenter-collection','用户收藏','{title} - {sitename}','{keywords}','{description}'),",
		"('22','ucenter-fans','用户粉丝','{title} - {sitename}','{keywords}','{description}'),",
		"('23','ucenter-follow','用户关注','{title} - {sitename}','{keywords}','{description}');",
	}
	if _, err := orm.NewOrm().Raw(strings.Join(sqlslice, "")).Exec(); err != nil {
		beego.Error(err.Error())
	}
}

func initLinks() {
	sqlslice := []string{"insert ignore into `" + conf.GetDatabasePrefix() + "friend_link_type`(`id`,`name`,`sort`) values ('1','友情链接','0'),",
		"('2','底部导航','1'),",
		"('3','顶部导航','2');",
	}
	if _, err := orm.NewOrm().Raw(strings.Join(sqlslice, "")).Exec(); err != nil {
		beego.Error(err.Error())
	}

	//友情链接

	sqlslice = []string{"insert ignore into `" + conf.GetDatabasePrefix() + "friend_link`(`id`,`title`,`link`,`type`,`Sort`) values ",
		"('1','开源','https://gitee.com/TruthHun/BookStack','3','0'),",
		"('2','赞助书栈','/read/help/support','2','0'),",
		"('3','使用帮助','/read/help/help','2','0'),",
		"('4','关于我们','/read/help/about','2','0'),",
		"('100','联系我们','/read/help/contact','2','0');",
	}
	if _, err := orm.NewOrm().Raw(strings.Join(sqlslice, "")).Exec(); err != nil {
		beego.Error(err.Error())
	}

}
